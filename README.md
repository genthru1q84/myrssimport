# LeeMe #


La configuración está al principio del archivo App.java .


Este proyecto os servirá como esqueleto para vuestro RSSImportador.

El proyecto, está hecho con Maven ( para así poder importar las librerias para conectarnos a SQL Server.)

También tenéis creado un test Unitario con JUNIT de ejemplo y he configurado los Bitbucket Pipeline para que cada vez que hagáis un push a master, pase automaticamente los test JUNIT.


## BBDD Con Docker ##

Debido a los problemas encontrados con la instalación de SQL Server Express en lugar de la Profesional y la imposibilidad de conectaros con la app java en condiciones, He añadido una configuración en docker para que arranqueis de forma casi automatica un SQL Server del bueno.

Para hacerlo, primero deberéis instalar Docker en vuestras maquinas. https://www.docker.com/get-started

Una vez echo esto, clonar este repositorio en vuestro equipo, y ejecutar el comando desde la carpeta donde esté este proyecto siguiente para levantar el servicio SQL Server

docker-compose up -d

Para apagar el servidor:

y docker-compose down 


La contraseña de sa y el puerto, son los que están configurados en el proyecto.

También he creado una mini base de datos del arepazo con un solo registro en la tabla clientes. En este mini Arepazo, solo tendréis la tabla clientes.

La App en Java, al arrancar mira si existe la bbdd predefinida, y si no, la crea y crea las tablas del script CrearBBDD.sql donde estarán los create tables necesarios. 

Al arrancar, se os generará una carpeta data, donde se guardaran esas bbdd. Esa carpeta, no se compartirá por GIT.

El miercoles os explico todo esto.
## Conexión con la BBDD ##
Tenéis una clase llamada BBDD que en el constructor recibís los parametros de la BBDD.

Porfavor, localizar esos parametros al principio de vuestra clase principal para luego poder localizarlos y modificarlos con mi bbdd.

Esa clase tiene 3 métodos.

* public int leerBBDD(String query,int n)
* public String leerBBDDUnDato(String query)
* int modificarBBDD(String query)

### leerBBDD ###

Nos pintará por consola una consulta con n campos. Nos devolverá el numero de filas de nuestra consulta. -1 si error.


### leerBBDDUnDato ###

Nos devolverá un string con el primer valor de la consulta.

### modificarBBDD ###

os lanzará una modificación (insert,delete,update) a la bbdd y nos devolverá el numero de registros afectados, -1 si da error


## Leer un XML con XPATH ##

Os he creado una clase llamada XMLParse, que dada una sentencia XPATH y una url, nos devolverá todos los elementos de esa sentencia. la sentencia XPATH deberá acabar con el text() para devolver el elemento textual, no un subXML.

Nos devolverá una lista de Strings.




## Maven ##

Debéis instalar Maven en vuestros equipos para que os funcione la parte de BBDD.