package com.cesur;
import java.util.List;
import java.util.Scanner;

import java.util.Locale;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
/**
 * Hello world!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="FinalBBDD";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    public static String url ="https://lenguajedemarcasybbdd.wordpress.com/feed";
    public static XmlParse x =new XmlParse();
    public static bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
    /** 
     * @param args
     */
    public static void main( String[] args )
    {   
        CrearBBDD();
        interfaz();
    }

    public static void mostrarPosts(int numero) {
        if(numero == 0){
            System.out.println("\n - Mostrando todos los posts:");
            b.leerBBDD("select Id, Fecha, Titulo from posts order by id desc", 3);
        } 
        else{
            System.out.println("\n - Mostrando "+numero+" posts:");
            b.leerBBDD("select top "+numero+" Id, Fecha, Titulo from posts order by id desc", 3);
        } 
    }

    public static void postDetalle(int pid){
        b.leerBBDD("select Titulo from posts where Id = "+pid, 1);
        b.leerBBDD("select Autor, Fecha, Categoria from posts where Id = "+pid, 3);
        b.leerBBDD("select Contenido from posts where Id = "+pid, 1);
    }

    public static void importarRSS(){
        List<String>  titulos = x.leer("//channel/item/title/text()",url);
        List<String>  contenidos = x.leer("//channel/item/encoded/text()",url);
        List<String>  fechas = x.leer("//channel/item/pubDate/text()",url);
        List<String>  autores = x.leer("//channel/item/creator/text()",url);
        List<String>  categorias = x.leer("//channel/item/category/text()",url);
        for (int i = 0; i<titulos.size(); i++) {
            b.modificarBBDD("insert into posts values ('"+titulos.get(i)+"','"+contenidos.get(i)+"','"+categorias.get(i)+"','"+fechas.get(i)+"', '"+autores.get(i)+"' )");
        }
    }

    
    public static void postCateg(String categ){
        System.out.println("\n - Mostrando posts de la categoría "+categ+":");
        b.leerBBDD("select Id, Fecha, Titulo from posts where Categoria = '"+categ+"' order by id desc", 3);
    }

    
    public static void postSearch(String sear){
        System.out.println("\n - Mostrando posts que incluyan "+sear+":");
        b.leerBBDD("select Id, Fecha, Titulo from posts where Contenido Like '%"+sear+"%' order by id desc", 3);
    }

    public static void interfaz(){
        int opcion;
        int opcion2;
        String opcion3;
        do{ 
            System.out.println("\n0: Importar RSS.");
            System.out.println("1: Mostrar 10 últimos posts.");
            System.out.println("2: Ver todos los posts.");
            System.out.println("3: Post en detalle.");
            System.out.println("4: Filtrar por categoria.");
            System.out.println("5: Busca.");
            System.out.println("6: Nada más.");

            opcion = pedirOpcion(0,6);
            switch(opcion){
                case 0:
                    importarRSS();
                    break;
                case 1:
                    mostrarPosts(10);
                    break;
                case 2:
                    mostrarPosts(0);
                    break;
                case 3:
                    System.out.println("Introduce la ID del post.");
                    opcion2 = pedirOpcion(0,100);
                    postDetalle(opcion2);
                    break;
                case 4:
                    System.out.println("Introduce una categoría de entre:");
                    b.leerBBDD("select distinct Categoria from posts", 1);
                    opcion3 = pedirTexto();
                    postCateg(opcion3);
                    break;
                case 5:
                    System.out.println("Introduce algo para buscar:");
                    opcion3 = pedirTexto();
                    postSearch(opcion3);
                    break;
              
                case 6:
                    System.out.println("Hasta luego, muchas gracias.");
                    break;

            }
        }while(opcion!=6);

    }

    public static int pedirOpcion(int desde,int hasta){
        int x;
        final Scanner sc = new Scanner(System.in);
        do{
           x = sc.nextInt();
           if(x<desde || x>hasta){
               System.out.println("Debe estar entre "+desde+" y "+hasta);
           }
        }while(x<desde || x>hasta);
    return x;
    }

    
    public static String pedirTexto(){
        String x;
        final Scanner sc = new Scanner(System.in);
        x = sc.nextLine();
        return x;
    }


    private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0"; 
        if (i.equals("0")){
            System.out.println("si");
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
            b.modificarBBDD(query);
            b.modificarBBDD("CREATE TRIGGER faltaCategoria ON Posts FOR INSERT AS BEGIN UPDATE Posts SET Categoria = 'Sin categoria' WHERE Categoria = 'Uncategorized' OR Categoria = '' END");      
        }
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
}
