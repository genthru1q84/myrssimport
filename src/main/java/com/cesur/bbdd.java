package com.cesur;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;


/**
 * Clase para conectaros a un SQL Server. Referencia:
 * https://sqlchoice.azurewebsites.net/en-us/sql-server/developer-get-started/java/ubuntu/step/2.html
 */
public class bbdd 
{
    private   String connectionUrl ="";
    
    /** 
    * Constructor donde se pasan los datos de conexión a la bbdd SQL Server
    * @param server
    * @param port
    * @param database
    * @param user
    * @param password
    */
    public bbdd(String server,String port, String database, String user, String password)
    {
        connectionUrl= "jdbc:sqlserver://"+server+":"+port+";databaseName="+database+";user="+user+";password="+password;
        System.out.println(connectionUrl);
    }

    
    /**
     * Pasándole una select y el numero de columnas de esa select, se muestra por consola los datos. 
     * @param query
     * @param numcampos
     */
    public int leerBBDD(String query,int numcampos) {
     int contador = 0;
        try (Connection connection = DriverManager.getConnection(connectionUrl);) {
            try (Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(query)) {
                while (resultSet.next()) {

                    String linea = "";
                    for(int i=1;i<=numcampos;i++){
                        contador++;
                        linea =linea+ " " + resultSet.getString(i) ; 
                    }
                    System.out.println(linea);
                }
            }
            connection.close();
        }
        // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
            contador=-1;
        }
        return contador;
    }

    
    /** 
     * Se le pasa una select que devuelva un solo campo, nos devolverá ese campo siempre en un string.
     * @param query
     * @return String
     */
    public String leerBBDDUnDato(String query) {
        String ret="";
        try (Connection connection = DriverManager.getConnection(connectionUrl);) {
            try (Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(query)) {
                while (resultSet.next()) {
                    ret= resultSet.getString(1) ; 
                }
            }
            connection.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
    
    /** 
     * Se le pasa una instrucción insert, update y delete. devuelve el num de filas afectadas. Devuelve -1 si da error.
     * @param query
     * @return int
     */
    public int modificarBBDD(String query) {
        int rowsAffected =0;
        try (Connection connection = DriverManager.getConnection(connectionUrl);) {
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                 rowsAffected = statement.executeUpdate();
            }
            connection.close();
        }
        catch (Exception e) {
            rowsAffected =-1;
            e.printStackTrace();
        }
        return rowsAffected;
    }
}
